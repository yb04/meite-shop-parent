package com.mayikt.common.base;

import lombok.Data;

import java.io.Serializable;


@Data
public class BaseResponse<T> implements Serializable {
	private static final long serialVersionUID = -8469420879227883186L;

	/**
	 * 返回码
	 */
	private String code = "001000";
	/**
	 * 消息
	 */
	private String msg;
	/**
	 * 返回
	 */
	private T data;


	public BaseResponse() {

	}

	public BaseResponse(ResultCode resultCode) {
		super();
		this.code = resultCode.code()+"";
		this.msg = resultCode.message();
	}

	public BaseResponse(String msg, T data) {
		super();
		this.msg = msg;
		this.data = data;
	}

	public BaseResponse(String code, String msg, T data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}


	public static <T> BaseResponse ok(T data) {
		BaseResponse<T> commonResponse = new BaseResponse<>();
		commonResponse.setMsg("success");
		commonResponse.setData(data);
		return commonResponse;
	}

	public static <T> BaseResponse ok() {
		BaseResponse<T> commonResponse = new BaseResponse<>();
		commonResponse.setMsg("success");
		return commonResponse;
	}

	public static <T> BaseResponse error(String msg) {
		BaseResponse<T> commonResponse = new BaseResponse<>();
		commonResponse.setMsg(msg);
		commonResponse.setCode("9999");
		return commonResponse;
	}

}
