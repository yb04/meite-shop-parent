package com.mayikt.api.pay.service;

import com.alibaba.fastjson.JSONObject;
import com.mayikt.common.base.BaseResponse;
import com.mayikt.pay.input.dto.PayCratePayTokenDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 
 * 
 * 
 * @description: 支付交易服务接口

 */
@FeignClient(value = "APP-MAYIKT-PAY")
public interface PayMentTransacTokenService {

	/**
	 * 创建支付令牌
	 * 
	 * @return
	 */
	@GetMapping("/cratePayToken")
	public BaseResponse<JSONObject> cratePayToken(@Validated PayCratePayTokenDto payCratePayTokenDto);

}
