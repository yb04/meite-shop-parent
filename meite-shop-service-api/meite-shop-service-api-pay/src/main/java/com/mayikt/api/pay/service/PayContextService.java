package com.mayikt.api.pay.service;

import com.alibaba.fastjson.JSONObject;
import com.mayikt.common.base.BaseResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "APP-MAYIKT-PAY")
public interface PayContextService {
    @GetMapping("/toPayHtml")
    public BaseResponse<JSONObject> toPayHtml(@RequestParam("channelId")String channelId,
                                              @RequestParam("payToken")String payToken);
}
