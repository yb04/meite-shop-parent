package com.mayikt.api.product.service;

import com.mayikt.common.base.BaseResponse;
import com.mayikt.product.output.dto.ProductDto;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public interface ProductSearchService {
    @GetMapping("/search")
    public BaseResponse<List<ProductDto>> search(String name);
}
