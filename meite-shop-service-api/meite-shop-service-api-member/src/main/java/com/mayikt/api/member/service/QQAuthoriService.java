package com.mayikt.api.member.service;


import com.alibaba.fastjson.JSONObject;
import com.mayikt.common.base.BaseResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "APP-MAYIKT-MEMBER")
public interface QQAuthoriService {

    /**
     * 根据 openid查询是否已经绑定,如果已经绑定，则直接实现自动登陆
     *
     * @param qqOpenId
     * @return
     */
    BaseResponse<JSONObject> findByOpenId(@RequestParam("qqOpenId")String qqOpenId);
}
