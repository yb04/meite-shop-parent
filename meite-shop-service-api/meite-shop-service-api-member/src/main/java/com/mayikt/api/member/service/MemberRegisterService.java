package com.mayikt.api.member.service;


import com.alibaba.fastjson.JSONObject;
import com.mayikt.common.base.BaseResponse;
import com.mayikt.member.input.dto.UserInpDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Api(tags = "会员注册接口")
@FeignClient(value = "APP-MAYIKT-MEMBER")
public interface MemberRegisterService {

    /**
     * 用户注册接口
     */
    @PostMapping("/register")
    @ApiOperation(value = "会员用户注册信息接口")
    BaseResponse<JSONObject> register(@RequestBody UserInpDTO userInpDTO,
                                      @RequestParam("registerCode") String registCode);
}
