package com.mayikt.api.auth.service;


import com.alibaba.fastjson.JSONObject;
import com.mayikt.common.base.BaseResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description:  用户授权接口
 * @param:
 * @return:
 * @author: yangbo
 * @date: 2020/2/26 10:43
 */

@FeignClient(value = "APP-MAYIKT-AUTH")
public interface AuthorizationService {
    /**
     * 机构申请 获取appid 和appsecret
     */
    @GetMapping("/applyAppInfo")
    public BaseResponse<JSONObject> applyAppInfo(@RequestParam("appName") String appName);

    /**
     * 使用appid和appsecret密钥获取AccessToken
     *
     */
    public  BaseResponse<JSONObject> getAccessToken(@RequestParam("appId") String appId,
                                                    @RequestParam("appSecret") String appSecret);


    /**
     *验证Token是否失效
     */
    public BaseResponse<JSONObject> getAppInfo(@RequestParam("accessToken")String accessToken);
}


