package com.mayikt.zuul.gateway.handler.impl;

import com.mayikt.zuul.gateway.handler.GatewayHandler;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Component
@Slf4j
public class ToVerifyMapHandler extends BaseHandler implements GatewayHandler {

	@Override
	public void service(RequestContext ctx, HttpServletRequest req, HttpServletResponse response) {
		log.info(">>>>>>>第三关,api验证签名Handler执行>>>>");
		nextGatewayHandler.service(ctx, req, response);
	}

	@Override
	public void setNextHandler(GatewayHandler gatewayHandler) {

	}

}
