package com.mayikt.zuul.gateway.handler.impl;

import com.mayikt.zuul.gateway.handler.GatewayHandler;
import com.netflix.zuul.context.RequestContext;

public class BaseHandler {

    public GatewayHandler nextGatewayHandler;
    public void setGatewayHandler(GatewayHandler nextGatewayHandler){
        this.nextGatewayHandler=nextGatewayHandler;
    }
    public void resultError(Integer code, RequestContext ctx,String errorMsg){
        ctx.setResponseStatusCode(code);
        //网关相应为false,不会转发服务
        ctx.setSendZuulResponse(false);
        ctx.setResponseBody(errorMsg);
        ctx.getResponse().setContentType("text/html;charset=UTF-8");
    }
}
