package com.xxl.sso.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XxlSsoServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(XxlSsoServerApplication.class, args);
    }

}
