package com.mayikt.elk.aop.error;


import com.alibaba.fastjson.JSONObject;
import com.mayikt.elk.kafka.KafkaSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @description: 全局捕获异常
 */
@ControllerAdvice
@Slf4j
public class ElkGlobalExceptionHandler {
    @Autowired
    private KafkaSender<JSONObject> kafkaSender;

    public JSONObject exceptionHandler(Exception e){
        log.info("###全局捕获异常###,error:{}", e);
    //1.封装异常日志信息
        JSONObject errorJson=new JSONObject();
        JSONObject logJson=new JSONObject();
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        logJson.put("request_time",df.format(new Date()));
        logJson.put("errpr_info",e);
        errorJson.put("error",logJson);
        kafkaSender.send(errorJson);
        //2.返回错误信息
        JSONObject result=new JSONObject();
        result.put("code",500);
        result.put("msg","错误信息");

        return result;


    }
}
