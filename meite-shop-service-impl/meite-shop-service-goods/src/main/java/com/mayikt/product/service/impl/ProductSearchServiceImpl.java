package com.mayikt.product.service.impl;

import com.mayikt.api.product.service.ProductSearchService;
import com.mayikt.common.base.BaseApiService;
import com.mayikt.common.base.BaseResponse;
import com.mayikt.product.output.dto.ProductDto;

import java.util.List;

public class ProductSearchServiceImpl extends BaseApiService<List<ProductDto>> implements ProductSearchService {
    @Override
    public BaseResponse<List<ProductDto>> search(String name) {
        return null;
    }
}
