package com.mayikt.weixin.service;

import com.alibaba.fastjson.JSONObject;
import com.mayikt.common.base.BaseResponse;

public interface VerificaCodeService {

    public BaseResponse<JSONObject> verificaWeixinCode(String phone, String weixinCode);
}
