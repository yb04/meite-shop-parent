package com.mayikt.auyh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuyhApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuyhApplication.class, args);
    }

}
